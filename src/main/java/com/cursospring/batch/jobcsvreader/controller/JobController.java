package com.cursospring.batch.jobcsvreader.controller;

import com.cursospring.batch.jobcsvreader.runner.JobRunner;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/run")
@AllArgsConstructor
public class JobController {

    private final JobRunner jobRunner;

    @GetMapping(value = "/job")
    public ResponseEntity<String> runJob() {
        jobRunner.runBatchJob();
        return ResponseEntity.ok("Job One submitted successfully");
    }
}
