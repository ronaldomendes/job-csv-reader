package com.cursospring.batch.jobcsvreader;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobcsvreader"})
public class JobCsvReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobCsvReaderApplication.class, args);
    }

}
